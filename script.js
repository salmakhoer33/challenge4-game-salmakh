class Play {
    constructor () {
        this.playerName = "PLAYER1"
        this.botName = "COM"
        this.playerOption;
        this.botOption;
        this.winner = ""
    }

    get getBotOption() {
        return this.botOption;
    }

    set setBotOption (option) {
        this.botOption = option;
    }



    get getPlayerOption () {
       return this.playerOption
    }


    set setPlayerOption (option) {
        this.playerOption = option;
    }

    botBrain () {
        const option = ["rock","paper","scissor"];
        const bot = option[Math.floor(Math.random() * option.length)];
        return bot;
       
    }



    winCalculation() {
      if(this.botOption == "paper" && this.playerOption ==  "scissor"){
        return this.winner = this.playerName
      }else if(this.botOption == "paper" && this.playerOption == "rock" ){
        return this.winner = this.botName;
      }else if (this.botOption == "scissor" && this.playerOption == "paper" ){
        return this.winner = this.botName;
      }else if(this.botOption == "scissor" && this.playerOption == "rock" ){
        return this.winner = this.playerName;
      }else if(this.botOption == "rock" && this.playerOption == "paper" ){
        return this.winner = this.playerName;
      }else if(this.botOption == "rock" && this.playerOption == "scissor" ){
        return this.winner = this.botName;
      }else {
        return this.winner = "DRAW"
      }
}

    matchResult() {
        if (this.winner != "DRAW"){
            return `${this.winner} WIN`;
        }else {
            return ` ${this.winner} `;
        }
    }

  
}


function rps (params) {
    const play = new Play();
    play.setPlayerOption = params;
    play.botOption = play.botBrain();
    play.winCalculation();

    const inGame= document.getElementById("inGame");
    const result= document.getElementById("result");
    inGame.textContent = "💤💤💤"
    result.textContent = "💤💤💤"

    setTimeout (() => {
        inGame.textContent =  `${play.getPlayerOption} VS ${play.getBotOption}`
        result.textContent =  play.matchResult();
    }, 800);


}